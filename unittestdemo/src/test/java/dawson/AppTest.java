package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void echo() {
        App app = new App();
        assertEquals("Method should return the input value", 5, app.echo(5));
    }

    @Test
    public void oneMore() {
        App app = new App();
        assertEquals("The oneMore method should return x + 1.",6, app.oneMore(5) );
    }
}
