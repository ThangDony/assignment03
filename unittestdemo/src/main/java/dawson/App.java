package dawson;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }

    public int echo(int x) {
        return x;
    }

    public int oneMore(int x) {
        // Intentional bug: Always return x instead of x + 1
        return x;
    }
}
